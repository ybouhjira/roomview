#ifndef ROOMMODEL_H
#define ROOMMODEL_H

#include <QAbstractTableModel>
#include <QList>

#include "room.h"

class RoomModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit RoomModel(QObject *parent = 0);
    

private:
    QList<Room> m_roomList;
    
};

#endif // ROOMMODEL_H
