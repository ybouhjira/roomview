#include "mainwindow.h"
#include "roomview.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_roomView(new RoomView)
{
    setCentralWidget(m_roomView);
}

MainWindow::~MainWindow()
{
    
}
