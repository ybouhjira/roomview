#include "room.h"

Room::Room(QList<QPair<QDate, QDate> > const& bookings) : m_bookings(bookings)
{
}

QList<QPair<QDate, QDate> > Room::bookings() const
{
    return m_bookings;
}

void Room::setBookings(const QList<QPair<QDate, QDate> > &bookings)
{
    m_bookings = bookings;
}
