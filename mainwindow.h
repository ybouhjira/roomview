#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
class RoomView;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:
    RoomView* m_roomView;
};

#endif // MAINWINDOW_H
