#ifndef ROOM_H
#define ROOM_H

#include <QList>
#include <QPair>

class Room
{
public:
    Room(QList<QPair<QDate, QDate> > const& bookings = QList<QPair<QDate, QDate> >());

    QList<QPair<QDate, QDate> > bookings() const;
    void setBookings(const QList<QPair<QDate, QDate> > &bookings);

private:
    QList<QPair<QDate, QDate> > m_bookings;
};

#endif // ROOM_H
