#-------------------------------------------------
#
# Project created by QtCreator 2013-07-28T23:58:34
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RoomView
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    roomview.cpp \
    roommodel.cpp \
    room.cpp

HEADERS  += mainwindow.h \
    roomview.h \
    roommodel.h \
    room.h

CONFIG += c++11
